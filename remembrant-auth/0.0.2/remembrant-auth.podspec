Pod::Spec.new do |s|
  s.name             = "remembrant-auth"
  s.version          = "0.0.2"
  s.summary          = "Remembrant auth part"
  s.description = <<-DESC
    Just some classy description
  DESC
  s.homepage         = "https://gitlab.com/iwazovsky/remembrant-auth.git"
  s.license          = { :type => "MIT" }
  s.author           = { "Konstantin Tukmakov" => "tukmakov.konstantin@gmail.com" }
  s.source           = { :git => "https://gitlab.com/iwazovsky/remembrant-auth.git", :tag => s.version }

  s.platform     = :ios, '14.0'
  s.source_files = "RemembrantAuth/**/*.{swift,h,m}"
  s.swift_version = "5.0"
end
