Pod::Spec.new do |s|
  s.name             = "remembrant-auth"
  s.version          = "0.0.2"
  s.summary          = "Remembrant auth part"
  s.homepage         = "https://gitlab.com/iwazovsky/remembrant-auth.git"
  s.license          = 'Code is MIT, then custom font licenses.'
  s.author           = { "Konstantin Tukmakov" => "tukmakov.konstantin@gmail.com" }
  s.source           = { :git => "https://gitlab.com/iwazovsky/remembrant-auth.git", :tag => s.version }

  s.platform     = :ios, '14.0'
  s.requires_arc = true

  s.source_files = "RemembrantAuth/**/*.{swift,h,m}"

  s.public_header_files = "RemembrantAuth/**/*.{h}"

  s.swift_version = "5.0"
end
